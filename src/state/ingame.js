/*global Phaser */
/*global game */
/*global scoreUser*/
/*global timerUser*/
/*global isWin*/
/*global gameLimit*/
/*global levelSetting*/
/*global totalLevel*/
/*global currentLevel*/

// NEW UPDATE PIXELNINE (ALL STYLE)
var style_object = {
    font: "12px Lato",
    fill: "#FFFFFF",
    boundsAlignH: "center",
    boundsAlignV: "top",
    fontWeight: "300",
    fontStyle:'normal'
};
var style = {
    font: "18px Lato",
    fill: "#FFFFFF",
    boundsAlignH: "left",
    boundsAlignV: "middle",
    fontWeight: "300",
    fontStyle:'normal'
};
var sub_hint_text_style = {
    font: "16px Lato",
    fill: "#FFFFFF",
    boundsAlignH: "left",
    boundsAlignV: "middle",
    fontWeight: "300",
    fontStyle:'normal'
};
var redStyle = {
    font: "50px Lato",
    fill:"#FF0000",
    boundsAlignH:"center",
    boundsAlignV:"middle"
};
var title_style = {
    font: "70px Lato",
    fill: "#FFFFFF",
    boundsAlignH: "center",
    boundsAlignV: "middle",
    fontWeight: "300",
    fontStyle:'normal'
};
var info_style = {
    font: "12px Lato",
    fill: "#FFFFFF",
    boundsAlignH: "center",
    boundsAlignV: "middle",
    fontWeight: "300",
    fontStyle:'normal'
};

var ingame = function(game) {
    console.log("%cStarting my awesome game", "color:white; background:red");
};

ingame.prototype = {
    preload: function() {
        this.game.scale.maxWidth = screen.width;
        this.game.scale.maxHeight = window.innerHeight;
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        this.ingameData = levelSetting[currentLevel];

        this.game.load.atlasJSONHash('UI', 'asset/UI/ui.png', 'asset/UI/ui.json');
        this.game.load.atlasJSONHash('GAME_ASSET', 'asset/' + this.ingameData.folder_dest + '/JsonData.png', 'asset/' + this.ingameData.folder_dest + '/JsonData.json');

        this.game.load.image("black_bg", "asset/image/Solid_black.png");

        this.game.load.image("button", "asset/image/blue_button00.png");
    },
    create: function() {
        scoreUser = 0;
        this.isUseHint = false;
        // teks info yang ada di sebelah bawah, teks teks petunjuk
        this.arrPanelObjects = [];
        // hidden object
        this.arrRandomObjects = [];
        this.isStart = false;
        //define init variable (seconds)
        this.longGameLimit = 120;
        this.thisScore = 0;
        //Create all assets
        this.timeNow = 0;
        this.allObjectData = this.ingameData.objectData;
        this.foundState = [];
        this.totalBarang = this.ingameData.objectToFind;
        this.totalHint = totalKoin_;
        this.ingameCoin = 1;
        //generate index 1-50 | 1 - totalobjectData
        this.AllIndexArray = [];
        this.selectedIndexArray = [];

        for (var i = 0; i < this.allObjectData.length; i++) {
            this.AllIndexArray.push(i);
        }

        for (var i = 0; i < this.AllIndexArray.length && i < this.ingameData.objectToFind; i++) {
            var randomIndex = Math.floor(Math.random() * this.AllIndexArray.length);
            this.selectedIndexArray.push(this.AllIndexArray[randomIndex]);
            this.AllIndexArray.splice(randomIndex, 1);
        }
        this.selectedIndexArray.sort(function(a, b) {
            return a - b
        });


        //create background
        var background = game.add.sprite(0, 0, 'GAME_ASSET', 'background.jpg');
        background.width = game.width;
        background.height = game.height;

        this.generateHiddenObjects();

        //create z indext for topest layer
        this.topest_z = game.add.graphics(0, 0);
        //create z index for where object to show
        this.reveal_hidden_object = game.add.graphics(0, 0);
        this.setReadyUI();


        //CreateListener

    },
    startGame: function(evt) {
        //remove ready asset
        this.black_bg.alpha = 0;
        this.black_bg.inputEnabled = false;
        game.world.remove(this.readyButton);
        game.world.remove(this.ReadyText);

        //start game
        this.isStart = true;

        //enable clicable buttons
        for (var i = 0; i < this.arrRandomObjects.length; i++) {
            this.arrRandomObjects[i].inputEnabled = true;
        }
        this.setUI();
    },
    showHint: function(evt) {
        if (this.isUseHint)
            return;

        this.isUseHint = true;
        //decrement total hint user
        this.totalHint--;
        this.ingameCoin--;

        this.hint_text.text = this.totalHint;
        //disable action hint button
        this.hint_btn.inputEnabled = false;
        this.hint_btn.tint = 0x555555;
        for (var k = 0; k < this.foundState.length; k++) {
            if (this.foundState[k] == false) {
                this.game.world.swap(this.black_bg, this.topest_z);
                this.game.world.swap(this.arrRandomObjects[k], this.reveal_hidden_object);
                this.indexShowedObject = k;
                break;
            }
        }

        //create tween make alpha 0.8 - idle 2 detik - make alpha 0 again
        var tween = this.game.add.tween(this.black_bg)
            .to({
                alpha: 0.8
            }, 500, Phaser.Easing.Linear.None)
            .to({}, 2000, Phaser.Easing.Linear.None)
            .to({
                alpha: 0
            }, 500, Phaser.Easing.Linear.None)
            .start();
        //invoke another function change z index to default position
        tween.onComplete.add(this.hideHint, this);

        console.log(this.foundState);
    },
    hideHint: function() {
        this.game.world.swap(this.black_bg, this.topest_z);
        this.game.world.swap(this.arrRandomObjects[this.indexShowedObject], this.reveal_hidden_object);
    },
    foundObject: function(evt) {
        //Bring object to the top
        //tween object, dari asal ke panel hint (bawah kiri)
        var tweenobjects = game.add.tween(evt).to({
                y: this.arrPanelObjects[evt.randomIndex].y - 50,
                x: this.arrPanelObjects[evt.randomIndex].x + this.arrPanelObjects[evt.randomIndex].width / 2 - 10,
                alpha: 0
            },
            1000, Phaser.Easing.Linear.None, true).start();
        tweenobjects.onComplete.add(this.checkWinState, this)
            //disable object agar tidak bisa di klik
        evt.inputEnabled = false;
        //decrement total barang
        this.totalBarang--;
        //tambah score
        this.thisScore += this.longGameLimit - Math.floor(this.timeNow / 1000);

        //fade out teks petunjuk
        this.score_text.text = this.thisScore;
        game.add.tween(this.arrPanelObjects[evt.randomIndex]).to({
            alpha: 0
        }, 1000, Phaser.Easing.Linear.None, true);
        //set found state
        this.foundState[evt.randomIndex] = true;

        //stop timer if hidden object all revealed
        if (this.totalBarang == 0)
            this.isStart = false;

    },
    checkWinState: function() {
        if (this.totalBarang == 0) {
            gameLimit = this.longGameLimit;
            console.log(scoreUser);
            scoreUser = this.thisScore;
            timerUser = Math.floor(this.timeNow / 1000);
            isWin = true;
            currentLevel++;

            totalKoin_ = this.ingameCoin;

            ajaxReq = $.ajax({
              type: 'POST',
              url: apiUrl+"/api/ho/game/score",
              dataType: "JSON",
              data:{
                id:id_,
                key:key_,
                score:scoreUser,
                coin:totalKoin_
              },
              success: function(resultData) {

                if(resultData.status == false)
                {
                  alert("ERROR");
                }else{
                  game.state.start("WinScreen");
                }
              }
            });

        }
    },
    update: function() {
        if (!this.isStart)
            return;

        this.timeNow += this.game.time.elapsed;

        var seconds = (this.longGameLimit - Math.floor(this.timeNow / 1000)) % 60;
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        var min = Math.floor((this.longGameLimit - Math.floor(this.timeNow / 1000)) / 60);
        this.time_text.text = min + ":" + seconds;

        if (this.timeNow / 1000 > this.longGameLimit) {
            this.timeNow = this.longGameLimit;
            game.state.start("WinScreen");
            timerUser = 0;
            scoreUser = this.thisScore;
            isWin = false;
        }
    },
    setReadyUI: function() {
        //create black background (appear at hint function)
        this.black_bg = game.add.sprite(0, 0, 'black_bg');
        this.black_bg.width = game.width;
        this.black_bg.height = game.height;
        this.black_bg.alpha = 0.9;
        this.black_bg.events.onInputDown.add(this.startGame, this);
        this.black_bg.inputEnabled = true;

        this.ReadyText = game.add.text(0, 0, this.ingameData.levelTitle, title_style);
        this.ReadyText.setTextBounds(0, 0, game.width, game.height);
    },
    generateHiddenObjects: function() {
        //generate random hidden objects
        for (i = 0; i < this.allObjectData.length; i++) {
            var randomObject = game.add.sprite(this.allObjectData[i].posX, this.allObjectData[i].posY, 'GAME_ASSET', (i + 1) + '.png');

            randomObject.index = i;

            for (var j = 0; j < this.selectedIndexArray.length; j++) {
                if (i == this.selectedIndexArray[j]) {
                    randomObject.randomIndex = j;
                    randomObject.events.onInputDown.add(this.foundObject, this);
                    this.arrRandomObjects.push(randomObject);
                    this.foundState.push(false);
                }
            }
        }
    },
    setUI: function() {
        this.uiBawah = game.add.sprite(0, 0, 'UI', 'UI bawah.png');
        this.uiBawah.anchor.setTo(0.5, 1);
        this.uiBawah.x = game.width / 2;
        this.uiBawah.y = game.height;

        this.score_text = game.add.text(0, 0, "0", style);
        this.score_text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
        this.score_text.setTextBounds(690, 10, 50, 20);

        this.level_text = game.add.text(344, 644, (currentLevel + 1), style);

        this.sub_hint_text = game.add.text(506, 645, this.ingameData.hint, sub_hint_text_style)

        this.time_text = game.add.text(320, 689, "0:0", redStyle);


        this.hint_btn = game.add.sprite(0, 0, 'button');
        this.hint_btn.width = 200;
        this.hint_btn.height = 100;
        this.hint_btn.alpha = 0;
        this.hint_btn.x = game.width - (this.hint_btn.width + 10);
        this.hint_btn.y = game.height - (this.hint_btn.height + 10);

        this.fb_btn = game.add.sprite(557, 0, "UI", "fb.png");
        this.fb_btn.anchor.setTo(0.5, 1);
        this.fb_btn.y = this.game.height;

        this.twitter_btn = game.add.sprite(743, 0, "UI", "twitter.png");
        this.twitter_btn.anchor.setTo(0.5, 1);
        this.twitter_btn.y = this.game.height;

        this.hint_btn = game.add.sprite(940, 0, "UI", "hint.png");
        this.hint_btn.anchor.setTo(0.5, 1);
        this.hint_btn.y = this.game.height;


        this.hint_text = game.add.text(429, 644, this.totalHint, style);

        for (i = 0; i < 10 && i < this.ingameData.objectToFind; i++) {
            var textObject;
            if (i >= 5) {
                textObject = game.add.text(465 + ((i - 5) * 130), 706, this.allObjectData[this.selectedIndexArray[i]].name, style_object);
            }
            else {
                textObject = game.add.text(465 + (i * 130), 683, this.allObjectData[this.selectedIndexArray[i]].name, style_object);
            }
            console.log(this.selectedIndexArray[i]);
            this.arrPanelObjects.push(textObject);
        };

        this.hint_btn.inputEnabled = true;
        this.hint_btn.tint = 0xFFFFFF;

        if (this.totalHint > 0) {
            this.hint_btn.events.onInputDown.add(this.showHint, this);
        }

    },
    makeDragAble: function(obj) {
        obj.inputEnabled = true;
        obj.input.enableDrag();
        obj.events.onDragStop.add(this.onDragStop, this);
    },
    onDragStop: function(sprite, pointer) {
        console.log(sprite.x, sprite.y);
    }

}